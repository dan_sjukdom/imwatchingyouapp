import { Form, Input, Button, Row, Col, Typography } from "antd"
import { Formik } from "formik"
import  * as Yup from "yup"
import style from "../../styles/Signup.module.css"

const validation_schema = Yup.object().shape({
    email: Yup.string().email().required().strip(),
    username: Yup.string().required().strip(),
    password: Yup.string().required().strip(),
    requiredPassword: Yup.string().required().strip()
})


const { Title } = Typography

export default function Signup() {

    const [form] = Form.useForm()

    return (
        <div className={style.container}>
        <div style={{
           width: "100%",
           backgroundColor: "GrayText",
        }}>
            <Row>
            <Col span={8}></Col>
            <Col span={8}>
            <Title 
                className={style.form__title}
                level={2}
            >
                Sign Up
            </Title>
            <Form 
                form={form}
                layout="vertical"
            >
                <Form.Item label="Email">
                    <Input />
                </Form.Item>
                <Form.Item label="Username">
                    <Input />
                </Form.Item>
                <Form.Item label="Password">
                    <Input type="password"/>
                </Form.Item>
                <Form.Item label="Consfirm Password">
                    <Input type="password"/>
                </Form.Item>
                <Form.Item>
                    <Button
                        style={{
                            width: "100%",
                        }}
                    >
                        Join
                    </Button>
                </Form.Item>
            </Form>
            </Col>
            <Col span={8}></Col>
            </Row>
            </div>
        </div>
    )
}
