import Link from "next/link"
import { Layout, Button } from "antd"
import style from "../styles/Home.module.css"

export default function Home() {
  return (
     <Layout style={{ height: "100vh" }}>
      <Layout.Sider>
        <p> I'm in the side bitch! </p>
      </Layout.Sider>
      <Layout.Content>
        <h1>
            Welcome to imWatchingYou
        </h1>
        <div className={style.buttons}>
          <Link href="/login">
              <Button
              color="primary"
              > Login </Button>
          </Link>
          <Link href="/signup">
              <Button
                color="primary"
              > Signup </Button>
          </Link>
        </div>
       </Layout.Content>
    </Layout>
  )
}